# Beamer Templates

This repository contains [`beamer`](https://www.overleaf.com/learn/latex/Beamer) themes for presentations and posters, themed loosely according to the [Corporate Design Guidelines of our University](https://uni-tuebingen.de/fakultaeten/wirtschafts-und-sozialwissenschaftliche-fakultaet/faecher/fachbereich-wirtschaftswissenschaft/fachbereich-wirtschaftswissenschaft/wirtschaftswissenschaft/einrichtungen-wirtschaftswissenschaft/it/faq/faq/corporate-design-cd/).
It has some more features than the [official template](https://uni-tuebingen.de/de/12641).

## 🍃 Overleaf Template

You can download Overleaf templates [here](https://tue-umphy.gitlab.io/templates/EKUT-beamer-theme).

Import it to Overleaf by creating a new project and selecting **Upload Project**.

## Examples

You can find examples under the `examples` directory.

To compile and view it, run `latexmk -pvc` in the respetive folder. Obviously, you'll have to have LaTeX installed. To be sure, install the full TeXLive distribution:

```bash
# for Debian-based systems
sudo apt update && sudo apt install '^texlive-.*'
# for Arch-based systems
sudo pacman -Syu texlive-most
```

If you don't like `latexmk` (e.g. because you prefer plain `pdflatex` compilation), make sure you set your `TEXINPUTS` to **recursively** include the `EKUT` directory in this repository:

```bash
export TEXINPUTS=/path/to/this/repo//: # <- don't forget the double slashes and the colon!
pdflatex *.tex # compile as usual
```

If you are using graphical LaTeX-editors (like TexMaker for example), check how you can set the `TEXINPUTS` there.

Ideally, you include this repository as a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) into the repository, where you track your LaTeX document and configure your LaTeX compilation tool to set the `TEXINPUTS` to a relative path to that submodule.
