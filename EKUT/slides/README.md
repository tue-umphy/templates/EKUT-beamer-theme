# EKUT-slides Beamer Theme

This is a beamer theme loosely following the Corporate Design of the Eberhard
Karls Universität Tübingen, Germany.

## How to use

See the example file in this repository under `examples/latex/EKUT-slides`.

## Features

- automatic resizing of title to fit the headline
- automatic margin adjustment to all available beamer `aspectratio`s

## LaTeX Macros

The `EKUT-slides` theme comes with a couple of useful LaTeX-commands to ease
the process of creating presentations.

### Automatically shrinking the slide content

If you have content in a slide that exceeds (or may exceed at different aspect
ratios) the page, simply wrap the content into

```tex
\begin{shrinkcontent}
...
\end{shrinkcontent}
```

The content is then automatically shrinked to the text area if needed.

### Having an image in the frame title

If you would like to have an image in the frame title that has the right size,
use

```tex
\imageintitle{/path/to/image/file.png}
```

The image is automatically centered vertically and it even works with the
automatic title resizing.
