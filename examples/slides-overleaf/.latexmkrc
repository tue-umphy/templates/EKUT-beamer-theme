# This latexmkrc file is necessary (especially on Overleaf) if you have LaTeX source files (classes, beamer themes, picture, bibliography files, etc. in SUBDIRECTORIES.

use Cwd qw/cwd/;

# Add the current working directory to the TEXINPUTS
$ENV{'TEXINPUTS'}= cwd . "//:$ENV{TEXINPUTS}";

# Search the bibliography wherever standard LaTeX files are
$ENV{'BIBINPUTS'}=$ENV{'TEXINPUTS'};
$ENV{'BSTINPUTS'}=$ENV{'BIBINPUTS'};
