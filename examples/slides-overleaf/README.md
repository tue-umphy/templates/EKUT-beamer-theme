# 🍃 Overleaf Template

This directory is a template to use this beamer theme on [Overleaf](https://overleaf.com) (or our own instance).

You can download a readily-made Overleaf template [here](https://tue-umphy.gitlab.io/templates/EKUT-beamer-theme/EKUT-slides-overleaf.zip).

Import it to Overleaf by creating a new project and selecting **Upload Project**.

<details>
<summary>Click to show manual template creation</summary>
Clone this repository onto your machine (no you can't click the ”download” button on GitLab, that drops the symbolic links unfortunately... 😔):

```bash
git clone git@gitlab.com:tue-umphy/templates/EKUT-beamer-theme.git
```

Then with your file browser navigate to the `examples/` folder.

There, right-click on the `slides-overleaf` folder and select **Create Archive** and make a ZIP file. If your file manager does not provide this option, find another way of zipping this folder.

If you open this ZIP-file, in the `beamer/` folder there should be all beamer template file, make sure that is the case.

Also, the hidden `.latexmkrc` must be included, otherwise it won't work on Overleaf.

Then create a new project on Overleaf by **Upload Project** and then selecting this created ZIP file.
</details>

## 🔨 Building

Run `latexmk` to build the PDF.

Alternatively:

```bash
# set TEXINPUTS to also look for files in subdirectories
TEXINPUTS="`pwd`//:$TEXINPUTS" pdflatex *.tex
```

If this complains that the beamer theme is not found, then LaTeX doesn't like following symbolic links. Better download the [Overleaf template](https://tue-umphy.gitlab.io/templates/EKUT-beamer-theme/EKUT-slides-overleaf.zip) then...
